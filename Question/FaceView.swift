//
//  FaceView.swift
//  Happy
//
//  Created by Mickaël Lanier on 13/04/2018.
//  Copyright © 2018 Mickaël Lanier. All rights reserved.
//

import UIKit

protocol FaceViewDataSource {
    func happinessForFaceView(sender: FaceView)-> Double?
}

@IBDesignable class FaceView: UIView {
    
    var dataSource : FaceViewDataSource?
    var scale : CGFloat = 0.9
    var smileH : CGFloat = 150


    @objc func scale(gesture: UIPinchGestureRecognizer){
        scale *= gesture.scale
        gesture.scale=1
    }
    
    
    var faceCenter : CGPoint {
        get {
            return convert(center, from: superview)
        }
    }
    var faceRadius : CGFloat {
        get {
            return 0.9 * min(bounds.size.width, bounds.size.height)/2
        }
    }

    @IBInspectable var lineWidth : CGFloat = 3 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var color : UIColor = UIColor.red
    
    /*Smile*/
    /*var smileCenter : CGPoint {
        get {
            return CGPoint(x: faceCenter.x - (faceRadius/2), y: faceCenter.y - (faceRadius/2))
        }
    }
    
    var smileRadius : CGFloat {
        get {
            
        }
    }*/
    
    
    
     override func draw(_ rect: CGRect) {
        
        let happiness = dataSource?.happinessForFaceView(sender: self)
        print("Happiness : \(happiness!)")
        var smile : CGFloat = CGFloat(happiness!)
        print("SMILE : \(smile)")
        
        switch happiness {
        case 1.0? :
            smile = CGFloat(150)
            print("CONTENT ! SMILE : \(smile)")
        case 0.0? :
            smile = CGFloat(100)
            print("NEUTRE ! SMILE : \(smile)")
        case (-1.0)? :
            smile = CGFloat(-30)
            print("PAS CONTENT ! SMILE : \(smile)")
        default:
            smile = CGFloat(150)
        }
        
        /* <Eyes> */
        var eyeRadius : CGFloat {
            get {
                return 0.2 * min(bounds.size.width, bounds.size.height)/2
            }
        }
        var eyeCenter1 : CGPoint {
            get {
                return CGPoint(x: faceCenter.x - (faceRadius/2), y: faceCenter.y - (faceRadius/2))
            }
        }
        
        var eyeCenter2 : CGPoint {
            get {
                return CGPoint(x: faceCenter.x + (faceRadius/2), y: faceCenter.y - (faceRadius/2))
            }
        }
        
        var pointA : CGPoint {
            get {
                return CGPoint(x:(faceCenter.x - (faceRadius/2)), y:(faceCenter.y + (faceRadius/2)))
            }
        }
        
        var pointB : CGPoint {
            get {
                return CGPoint(x:(faceCenter.x) - (pointA.x/3), y:(faceCenter.y) + smile)
            }
        }
        
        var pointC : CGPoint {
            get {
                return CGPoint(x:(faceCenter.x) + (pointA.x/3), y:(faceCenter.y) + smile)
            }
        }
        
        var pointD : CGPoint {
            get {
                return CGPoint(x:(faceCenter.x + (faceRadius/2)), y:(faceCenter.y + (faceRadius/2)))
            }
        }
        
        /* </Eyes> */
        
        let facePath = UIBezierPath(arcCenter: faceCenter, radius: faceRadius, startAngle: 0, endAngle: 2 * .pi, clockwise: true) //créer graphique vectorielle sur courbe de bézier (courbe contrôlées par pts de contrôle)
        let eyesPath1 = UIBezierPath(arcCenter: eyeCenter1, radius: eyeRadius, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        let eyesPath2 = UIBezierPath(arcCenter: eyeCenter2, radius: eyeRadius, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        
        /* ChangeBouche */
        let bouche = UIBezierPath()
        bouche.move(to: pointA)
        bouche.addCurve(to: pointD, controlPoint1: pointB, controlPoint2: pointC)
        bouche.lineWidth = lineWidth
        
        facePath.lineWidth = lineWidth
        eyesPath1.lineWidth = lineWidth
        eyesPath2.lineWidth = lineWidth
        
        color.set()
        facePath.stroke()
        eyesPath1.stroke()
        eyesPath2.stroke()
        bouche.stroke()
        
        
        
        
        //print("identifier : \()")
        //let smilePath = UIBezierPath(roundedRect: , byRoundingCorners: <#T##UIRectCorner#>, cornerRadii: <#T##CGSize#>)
        
    }
    
 

}
