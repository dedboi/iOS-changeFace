//
//  HappynessViewController.swift
//  Happy
//
//  Created by Mickaël Lanier on 13/04/2018.
//  Copyright © 2018 Mickaël Lanier. All rights reserved.
//

import UIKit

class HappynessViewController: UIViewController, FaceViewDataSource {
    func happinessForFaceView(sender: FaceView) -> Double? {
        return Double(happiness - 50)/50
    }
    
    @IBOutlet weak var faceView: FaceView! {
        didSet {
            faceView.dataSource = self
            faceView.addGestureRecognizer(UIPinchGestureRecognizer(target: faceView, action:#selector(faceView.scale(gesture:))))
            faceView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action:#selector(changeHappiness(gesture:))))
            //faceView.smileH = (happiness as? CGFloat)!
        }
    }
    
    var happiness : Int = 50 {
        didSet { // Observateur d'événements
            happiness = min(max(happiness, 0), 100)
            print("allegresse = \(happiness)")
        }
    }
    
    func updateUI(){
        title = "\(happiness)"
        faceView?.setNeedsDisplay()
    }
    
    @objc func changeHappiness(gesture:UIPanGestureRecognizer) {
        switch gesture.state {
        case .changed : fallthrough
        case .ended:
            happiness += Int(gesture.translation(in: faceView).y/6)
        default: break
        }
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
