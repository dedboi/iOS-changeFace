//
//  QuestionViewController.swift
//  Question
//
//  Created by Mickaël Lanier on 27/04/2018.
//  Copyright © 2018 Mickaël Lanier. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var destination = segue.destination as? UIViewController
        if let  navCon = destination as? UINavigationController {
            destination = navCon.visibleViewController
        }
        if let hvc = destination as? HappynessViewController {//hvc = happiness view controller 'as?' -> cast Utilisation du if let car exceptions sont des Nil donc ignorés
            if let identifier = segue.identifier {
                switch identifier {
                case "content": hvc.happiness = 100
                case "neutre": hvc.happiness = 50
                case "triste": hvc.happiness = 0
                default: hvc.happiness = 30
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

